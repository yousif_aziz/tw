<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require "vendor/autoload.php";

use App\Actions;
use App\View\Blade as BladeWrapper;

BladeWrapper::setDir(__DIR__);
$segemnts = @request()->segments();


switch (@$segemnts[0]) {
    case null:
        return Actions::indexOrSearch();
    case "user":
        return Actions::getUserInfo($segemnts[1]);
    case "hash":
        return Actions::getHash($segemnts[1]);
}


