<?php $__env->startSection("content"); ?>
    <div class="col-md-3 hidden-md"></div>
    <div class="col-md-6">
        <?php $__currentLoopData = $rows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="tweet-wrapper img-rounded">
                <div class="user-wrapper">
                    <div class="user-img">
                        <img src="<?php echo e($row->user->profile_image_url); ?>" class="img-rounded img-thumbnail img-responsive"/>
                    </div>
                    <div class="user-info">
                        <p class="user-name"><?php echo e($row->user->name); ?></p>
                        <p class="user-tag">
                            <a target="_blank" href="https://twitter.com/<?php echo e($row->user->screen_name); ?>">
                                <?php echo e("@".$row->user->screen_name); ?>

                            </a>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                </div>
                <div class="tweet-text"><?php echo \App\ServiceLayers\TweetModifier::tweeetText($row); ?></div>
                <div class="tweet-media"><?php echo \App\ServiceLayers\TweetModifier::media($row); ?></div>
                <div class="tweet-text"><?php echo e(date("H:i A - d M Y",strtotime($row->created_at))); ?></div>
                <hr/>
                <p>
                    <i class="glyphicon glyphicon-refresh"></i><?php echo e($row->retweet_count); ?>

                    <i class="glyphicon glyphicon-heart"></i><?php echo e($row->favorite_count); ?>

                </p>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <div class="col-md-3 hidden-md"></div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("layout", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>