@extends("layout")


@section("content")
    <div class="col-md-3 hidden-md"></div>
    <div class="col-md-6">
        @foreach($rows as $row)
            <div class="tweet-wrapper img-rounded">
                <div class="user-wrapper">
                    <div class="user-img">
                        <img src="{{$row->user->profile_image_url}}" class="img-rounded img-thumbnail img-responsive"/>
                    </div>
                    <div class="user-info">
                        <p class="user-name">{{$row->user->name}}</p>
                        <p class="user-tag">
                            <a target="_blank" href="https://twitter.com/{{$row->user->screen_name}}">
                                {{"@".$row->user->screen_name}}
                            </a>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                </div>
                <div class="tweet-text">{!! \App\ServiceLayers\TweetModifier::tweeetText($row) !!}</div>
                <div class="tweet-media">{!! \App\ServiceLayers\TweetModifier::media($row) !!}</div>
                <div class="tweet-text">{{date("H:i A - d M Y",strtotime($row->created_at))}}</div>
                <hr/>
                <p>
                    <i class="glyphicon glyphicon-refresh"></i>{{$row->retweet_count}}
                    <i class="glyphicon glyphicon-heart"></i>{{$row->favorite_count}}
                </p>
            </div>
        @endforeach
    </div>
    <div class="col-md-3 hidden-md"></div>
@stop