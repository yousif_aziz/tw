<!doctype html>
<html lang="en">
<head>
    <base href="/tweet/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Starter Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="public/css/bootstrap.min.css" rel="stylesheet">
    <link href="public/css/css.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="starter-template.css" rel="stylesheet">
</head>

<body>


<div class="navbar navbar-default navbar-static-top">
    <div class="container">
        <form>
            <div class="col-lg-12">
                <div class="form-group">
                    <label>search</label>
                    <input name="q" value="{{request()->q}}" class="form-control">
                </div>
            </div>
        </form>
    </div>
</div>

<div class="container">
    <div class="row">
        @foreach(\App\Errors\Error::get() as $err)
            <div class="alert alert-warning">{{$err->message}}</div>
        @endforeach
    </div>
    <div class="row">
        @yield("content")
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../../../../assets/js/vendor/popper.min.js"></script>
<script src="../../../../dist/js/bootstrap.min.js"></script>
</body>
</html>
