<?php

namespace App\ServiceLayers;
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 16/11/17
 * Time: 08:50 ص
 */

class TweetModifier
{
    public static function tweeetText($tweetObj)
    {
        $text = $tweetObj->text;
        $text = self::setHashs($text, @$tweetObj->entities->hashtags);
        $text = self::setUserMentions($text, @$tweetObj->entities->user_mentions);
        $text = self::setUrls($text, @$tweetObj->entities->urls);
        return $text;
    }

    private static function setHashs($text, $hashes = [])
    {
        foreach ($hashes as $has) {
            $text = str_replace("#" . $has->text, "<a href='?q=$has->text'># $has->text</a>", $text);
        }
        return $text;
    }private static function setUrls($text, $urls = [])
    {
        foreach ($urls as $url) {
            $text = str_replace( $url->url, "<a target='_blank' href='$url->expanded_url'>$url->url</a>", $text);
        }
        return $text;
    }

    private static function setUserMentions($text, $mentions = [])
    {
        foreach ($mentions as $men) {
            $text = str_replace("@" . $men->screen_name, "<a target='_blank' href='https://twitter.com/$men->screen_name'>@$men->screen_name</a>", $text);
        }
        return $text;
    }

    public static function media($tweet)
    {
        $medias = @$tweet->entities->media;
        if (!$medias) return "";
        $back = "<div style='width: 80%; margin: 5px auto'>";
        foreach ($medias as $media) {
            $back .= "<img src='$media->media_url' class='img-responsive img-rounded'/>";
        }

        return $back . "</div>";
    }
}
