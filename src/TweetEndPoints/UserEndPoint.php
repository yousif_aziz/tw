<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 16/11/17
 * Time: 09:39 ص
 */

namespace App\TweetEndPoints;


class UserEndPoint extends BaseEndPoint implements InterFaces\EndPointInterFace
{

    public $path = "users/lookup.json";

    public function getRequestMethod()
    {
        return "GET";
    }
}