<?php

namespace App\TweetEndPoints\InterFaces;
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/11/17
 * Time: 10:40 ص
 */

interface EndPointInterFace
{
//    public function path();

    public function getPath();

    public function setPath($path);

    public function setOptions(array $options);

    public function request();

    public function getRequestMethod();
}