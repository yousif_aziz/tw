<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/11/17
 * Time: 10:43 ص
 */

namespace App\TweetEndPoints;


abstract class BaseEndPoint
{
    public $path;
    public $options;
    private static $instance;

    /**
     * @return static
     */
    public static function instance()
    {
        return (self::$instance) ? self::$instance : (self::$instance = new static());
    }

    abstract public function getRequestMethod();

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions(array $options)
    {
        $this->options = $options;
        return $this;
    }

    public function getRequest()
    {
        return TweeterConnection::getConnection()
            ->setGetfield("?" . http_build_query($this->options))
            ->buildOauth(TweeterConnection::URL . $this->getPath(), $this->getRequestMethod())
        ;
    }

    /**
     * @return \TwitterAPIExchange
     */
    public function postRequest()
    {
        return TweeterConnection::getConnection()
            ->setPostfields($this->options)
            ->buildOauth(TweeterConnection::URL . $this->getPath(), $this->getRequestMethod());
    }

    public function request()
    {
        $response = (($this->getRequestMethod() == "GET") ? $this->getRequest() : $this->postRequest())->performRequest();
        return json_decode($response);
    }
}