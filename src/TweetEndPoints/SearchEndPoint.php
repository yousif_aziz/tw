<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/11/17
 * Time: 10:44 ص
 */

namespace App\TweetEndPoints;


class SearchEndPoint extends BaseEndPoint implements InterFaces\EndPointInterFace
{

    public $path = "search/tweets.json";

    public function getRequestMethod()
    {
        return "GET";
    }


}