<?php

namespace App\Errors;
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 16/11/17
 * Time: 07:59 ص
 */

class Error
{
    private static $srr = [];

    public static function set($err)
    {
        self::$srr = $err;
    }

    public static function get()
    {
        return self::$srr;
    }
}