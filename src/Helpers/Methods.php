<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/11/17
 * Time: 02:17 م
 */

use Illuminate\Http\Request;

/**
 * @param array $data
 */
function res_json($data)
{
    header('Content-Type: application/json;charset=utf-8');
    echo json_encode($data);
    die();
}

/**
 * @return Request
 */
function request()
{
    return Request::capture();
}