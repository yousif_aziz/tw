<?php

namespace App\View;
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/11/17
 * Time: 02:38 م
 */
use Philo\Blade\Blade as OrginalBlade;
class Blade
{
    private static $engine;
    private static $dir;

    private static function getBlade()
    {

        if (self::$engine) return self::$engine;


        $views = self::$dir . '/html/views';
        $cache = self::$dir . '/html/cache';

        return self::$engine = new OrginalBlade($views, $cache);

    }

    public static function setDir($dir)
    {
        self::$dir = $dir;
    }

    public static function view($view, $data = [])
    {
        echo self::getBlade()->view()->make($view, $data)->render();
        return;
    }
}