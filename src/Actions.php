<?php

namespace App;

use App\Errors\Error;
use App\TweetEndPoints\SearchEndPoint;
use App\TweetEndPoints\UserEndPoint;
use App\View\Blade as BladeWrapper;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 16/11/17
 * Time: 09:28 ص
 */
class Actions
{
    public static function indexOrSearch()
    {
        $res = SearchEndPoint::instance()->setOptions(["q" => request()->q])->request();
//        $res = json_decode(file_get_contents("download.json"));
//        res_json($res->statuses);

        if (@$res->errors) {
            Error::set($res->errors);
            $result = [];
        } else {
            $result = $res->statuses;
        }
        BladeWrapper::view("index", ["rows" => $result]);
    }

    public static function getUserInfo($userId)
    {
      $rs =   UserEndPoint::instance()->setOptions(["user_id" => $userId])->request();
      res_json($rs);
        return;
    }

    public static function getHash($hash)
    {
        echo $hash;
        return;
    }
}